<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogDb;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/','welcome')->name('home');

Route::view('/contact','contact')->name('contact');

Route::get('/blog',[BlogDb::class,'titulo'])->name('blog'); 

Route::view('/about','about')->name('about'); 

