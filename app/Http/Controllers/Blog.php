<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Blog extends Controller
{
    public function titulo(){
        $posts=[
            ['title' => 'Primer titulo'],
            ['title' => 'Sugndo titulo'],
            ['title' => 'Tercero titulo'],
            ['title' => 'Cuarto titulo'],
        ];
        return view('blog', ['posts'=>$posts]);
    }
}
