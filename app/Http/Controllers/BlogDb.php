<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;

class BlogDb extends Controller
{
    public function titulo(){
        $posts=Post::get();
        return view('blog', ['posts'=>$posts]);
    }
}
