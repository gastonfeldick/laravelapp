<?php

namespace App\Http\Controllers;

class About{
    public function titulo(){
        $posts=[
            ['title' => 'Primer titulo'],
            ['title' => 'Sugndo titulo'],
            ['title' => 'Tercero titulo'],
            ['title' => 'Cuarto titulo'],
        ];
        return view('blog', ['posts'=>$posts]);
    }
}

